package openesc;
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * @author Javier Hernández
 * 
 **/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import openesc.utils.I18n;
 
public class OpenESCApp extends Application {
    private final static Logger LOGGER = Logger.getLogger(OpenESCApp.class.getName());
    
    private Preferences prefs = Preferences.userNodeForPackage(openesc.OpenESCApp.class);
    public Preferences getPrefs() {
		return prefs;
	}
	
	// PREFERENCES //
	final String PREF_TARGET = "TARGET";
	final String PREF_CONNTYPE = "CONNTYPE";

	private OpenESCView view;

	private OpenESCController controller;

	@Override
    public void start(Stage stage) {				
    	// Initialize Locale for I18N
		File file = new File("lang.properties");
		FileInputStream is = null;
		if(file != null){
			try {
				is = new FileInputStream(file);
			} catch (FileNotFoundException e1) {
				LOGGER.log(Level.SEVERE, null, e1);
			}
		}
		if(is != null){
			Properties props = new Properties();
			try {
				props.load(is);
				switch (props.getProperty("lang")) {
				case "English":
					Locale.setDefault(new Locale("en", "EN"));
					break;
				case "Spanish":
					Locale.setDefault(new Locale("es", "ES"));
					break;
				case "French":					
					break;
				default:
					break;
				}
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, null, e);
			}
		} else {
			Locale.setDefault(new Locale("en", "EN")); 
		} //end Locale init for I18N.
		
		
		// Initialize the Main view.
		view = new OpenESCView();
		controller = new OpenESCController(view, this);
		
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(view, visualBounds.getWidth(), visualBounds.getHeight());
        scene.getStylesheets().add(getClass().getResource("styles/openesc.css").toExternalForm());
        
        PerspectiveCamera camera = new PerspectiveCamera();
        camera.setFieldOfView(10);
        scene.setCamera(camera);
        
        stage.setTitle(I18n.COMMON.getString("app.title"));
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
    } // end Start method.
		
	
	// ************* Preferences Methods *************** //
	public void setTarget(String string) {
		prefs.put(PREF_TARGET, string);	
	}
	public String getTarget() {
		return prefs.get(PREF_TARGET, null);
	}	
	public void setConnectionType(String string) {
		prefs.put(PREF_CONNTYPE, string);	
	}
	public String getConnectionType() {
		return prefs.get(PREF_CONNTYPE, null);
	}

	public OpenESCController getController() {
		return controller;
	}

	public void setController(OpenESCController controller) {
		this.controller = controller;
	}	
}
