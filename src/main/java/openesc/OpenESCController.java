package openesc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;
import java.util.logging.Logger;

import com.fazecast.jSerialComm.SerialPort;

import eu.hansolo.enzo.experimental.pushbutton.PushButton.SelectionEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import openesc.cli.CLIController;
import openesc.cli.CLIModel;
import openesc.cli.CLIView;
import openesc.firmware.FirmwareController;
import openesc.firmware.FirmwareModel;
import openesc.firmware.FirmwareView;
import openesc.motors.MotorsController;
import openesc.motors.MotorsModel;
import openesc.motors.MotorsView;
import openesc.setup.SetupController;
import openesc.setup.SetupModel;
import openesc.setup.SetupView;
import openesc.utils.SerialCommunication;
	
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *   
 * @author Javier Hernández
 * 
 **/

public class OpenESCController {
	private final static Logger LOGGER = Logger.getLogger(OpenESCController.class.getName());
	
	private OpenESCView mainView;
	private OpenESCApp  fxApp;

	public FirmwareView fview;
	public SetupView setup_view;
	public MotorsView mview;

	public OpenESCController(OpenESCView view, OpenESCApp model){
		this.mainView = view;
		this.fxApp = model;
		
		// Tell the View that when ever the Help button is
		// clicked to execute the ActionEvent method 
		// in the Event4btnHelp inner class.
		this.mainView.addActionEvent4btnHelp(new Event4btnHelp());
		
		// Tell the View that when ever the Language
		// button is clicked to execute the ActionEvent
		// method in the Event4btnLanguage inner class.
		this.mainView.addActionEvent4btnLanguage(new Event4btnLanguage());
		
		// Tell the View that when ever the Firmware
		// button is clicked to execute the ActionEvent
		// method in the Event4btnFirmware inner class.
		this.mainView.addActionEvent4btnFirmware(new Event4btnFirmware());
		
		// Tell the View that when ever the Motors
		// button is clicked to execute the ActionEvent
		// method in the Event4btnMotors inner class.
		this.mainView.addActionEvent4btnMotors(new Event4btnMotors());
		
		// Tell the View that when ever the Setup
		// button is clicked to execute the ActionEvent
		// method in the Event4btnSetup inner class.
		this.mainView.addActionEvent4btnSetup(new Event4btnSetup());
		
		// Tell the View that when ever the Connection
		// button is clicked to execute the ActionEvent
		// method in the Event4btnConnection inner class.
		this.mainView.addActionEvent4btnConnection(new Event4btnConnect());
	}
	
		
	// Action Event for the Button Help of the Menu
    class Event4btnHelp implements EventHandler<ActionEvent> {
    	@Override
		public void handle(ActionEvent event) {
			fxApp.getHostServices().showDocument("https://readthedocs.org/");
		}
    }
    
	/*
	 * // Action Event for the Button Donate of the Menu class Event4btnDonate
	 * implements EventHandler<ActionEvent> {
	 * 
	 * @Override public void handle(ActionEvent event) {
	 * fxApp.getHostServices().showDocument(
	 * "https://www.paypal.com/"
	 * ); } }
	 */
    
    // Action Event for the Button Language of the Menu
    class Event4btnLanguage implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			// Show dialog and Store selected language in properties file
			mainView.showLangDialog();
		}    	
    }
    
    // Action Event for the Button Connect of the Menu
    class Event4btnConnect implements EventHandler<ActionEvent> {
		private SerialPort comPort;
		private String nextPort;

		@Override
		public void handle(ActionEvent event) {
			//Serial connection
			SerialPort[] comPorts = SerialPort.getCommPorts();
			ArrayList<SerialPort> comPortsList = new ArrayList<>(Arrays.asList(comPorts));
			ListIterator<SerialPort> portIterator = comPortsList.listIterator();
	        while (portIterator.hasNext()) { 
	            nextPort = (String)portIterator.next().getPortDescription();				
	            String valueCombo = mainView.getComboPorts().getValue().toString();
	        	if(nextPort.toString().equals(valueCombo)){
					  comPort = portIterator.previous(); 
					  portIterator.next();
				  }
	        }
			SerialCommunication serial = new SerialCommunication();
			if(comPort!=null) {
				serial.connect(comPort);
				OpenESCView.disableMenuButtons(false);
			}
			
			
			
			/*
			 * final Notification notification; Notification.Notifier notifier;
			 * Notifier.INSTANCE.setAlwaysOnTop(false); notification =
			 * NotificationBuilder.create()
			 * .title(I18n.COMMON.getString("title.notify.connection"))
			 * .message(I18n.COMMON.getString("content.notify.connection"))
			 * .image(Notification.INFO_ICON) .build();
			 * 
			 * notifier = NotifierBuilder.create().build(); notifier.notify(notification);
			 */
		}    	
    }
    
    // Action Event for the Button Firmware of the Menu
    class Event4btnFirmware implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			// Show the Firmware Flasher Screen
			final FirmwareController f_controller;
			final FirmwareModel f_model;
			
			f_model = new FirmwareModel(fxApp.getPrefs());
			f_controller = new FirmwareController(f_model, mainView, fxApp);
			fview = new FirmwareView();
			f_controller.setFirmwareView(fview);
			mainView.setRight(fview.createRightGauges());
			f_controller.addFirmwareEvents();
		}    	
    }
   
    // Action Event for the Button Setup of the Menu
    class Event4btnSetup implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			// Show the Setup Screen
			final SetupModel setup_model;
			final SetupController setup_controller;
			
			setup_model = new SetupModel(fxApp.getPrefs());
			setup_controller = new SetupController(setup_model, mainView, fxApp);
			setup_view = new SetupView();
			setup_controller.setSetupView(setup_view);
			mainView.setRight(null);
			mainView.setCenter(setup_view.createSetupCenter(fxApp.getPrefs()));
			setup_controller.addSetupEvents();	
		}    	
    }
        
    // Action Event for the Button Motors of the Menu
    class Event4btnMotors implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			final MotorsModel mmodel;
			final MotorsController mcontroller;	
			
			// Clear the pane
			mainView.setRight(null);
			mainView.setCenter(null);
			
			// Show the Motors Screen
			mmodel = new MotorsModel();
			mcontroller = new MotorsController(mmodel, mainView, fxApp);
			mview = new MotorsView();
			mcontroller.setMotorsView(mview);
			//mainView.setRight(mview.createMotorsGauges());
			mainView.setCenter(mview.createMotorsGauges());
			mcontroller.addMotorsEvents();				
		}    	
    }      
 
    // Action Event for the Button CLI of the Menu
    class Event4btnCLI implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			// Show the CLI Screen
			final CLIModel cmodel;
			final CLIController ccontroller;
			
			cmodel = new CLIModel(fxApp.getPrefs());
			ccontroller = new CLIController(cmodel, mainView, fxApp);
			CLIView cview = new CLIView();
			ccontroller.setCLIView(cview);
			mainView.setRight(null);
			mainView.setCenter(cview.createCLICenter(fxApp.getPrefs()));
			ccontroller.addCLIEvents();				
		}    	
    }
    
      
    // ON Selection Event for the Push Button Connection of the ToolBar
    class OnSelection4btnConnection implements EventHandler<SelectionEvent> {

		@Override
		public void handle(SelectionEvent event) {
					//mainView.showConnDialog(new RoverConfigView());

			/*
			 * final Notification notification; Notification.Notifier notifier;
			 * 
			 * Notifier.INSTANCE.setAlwaysOnTop(false);
			 * 
			 * notification = NotificationBuilder.create()
			 * .title(I18n.COMMON.getString("title.notify.connection"))
			 * .message(I18n.COMMON.getString("content.notify.connection"))
			 * .image(Notification.INFO_ICON) .build();
			 * 
			 * notifier = NotifierBuilder.create().build(); notifier.notify(notification);
			 */
		}
	}
    
    // OFF Selection Event for the Push Button Connection of the ToolBar
    class Deselection4btnConnection implements EventHandler<SelectionEvent> {

		@Override
		public void handle(SelectionEvent event) {
			
			/*
			 * final Notification notification; Notification.Notifier notifier;
			 * 
			 * Notifier.INSTANCE.setAlwaysOnTop(false);
			 * 
			 * notification = NotificationBuilder.create()
			 * .title(I18n.COMMON.getString("title.notify.desconnection"))
			 * .message(I18n.COMMON.getString("content.notify.desconnection"))
			 * .image(Notification.INFO_ICON) .build();
			 * 
			 * notifier = NotifierBuilder.create().build(); notifier.notify(notification);
			 */
		}
    }
}
