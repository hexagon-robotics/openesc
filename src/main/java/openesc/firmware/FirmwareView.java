package openesc.firmware;
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * @author Javier Hernández
 * 
 **/
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import eu.hansolo.enzo.onoffswitch.OnOffSwitch;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.Gauge.SkinType;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.Section;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import openesc.utils.HorizonSquare;
import openesc.utils.I18n;

public class FirmwareView {
private final static Logger LOGGER = Logger.getLogger(FirmwareView.class.getName());
	
	private ToggleGroup telemetrySwitchToggleGroup;
	private ToggleButton btnTelemetry;
	private ToggleButton btnPreferences;
	private OnOffSwitch switch1;
	private OnOffSwitch switch2;
	private OnOffSwitch switch3;

	private GridPane gridFirmware;
	private Button btnClearWifi;
	private Button btnSubmitWifi;
	private Label labelHost;
	TextField tfHost;
	private Label labelPort;
	TextField tfPort;

	Gauge speed;
	Gauge barometer;
	Gauge temperature;
	Gauge gnss;
	Gauge battery;
	Gauge distance;
	HorizonSquare horizon;
	
	SplitPane splitPane4RightPane;


	public FirmwareView() {
		this.createRightGauges();
	}
	
	
	@SuppressWarnings("unchecked")
	public Node createRightGauges() {
		splitPane4RightPane = new SplitPane();
		splitPane4RightPane.setPrefSize(200, 60);
		splitPane4RightPane.setOrientation(Orientation.VERTICAL);
		
		horizon = new HorizonSquare();
		
		gnss =  GaugeBuilder.create()
				.prefSize(120, 120)
                .skinType(SkinType.SIMPLE_DIGITAL)
                .foregroundBaseColor(Color.web("#D6264F"))
                .barColor(Color.DARKRED)
                .unit("GPS")
                .animated(true)
                .build();
		
		barometer = GaugeBuilder.create()
                .skinType(SkinType.DASHBOARD)
                .value(24)
                .animated(true)
                .title("Baro")
                .unit("µm")
                .maxValue(40)
                .barColor(Color.web("#0697e0"))
                .valueColor(Color.web("#0697e0"))
                .titleColor(Color.web("#0697e0"))
                .unitColor(Color.web("#0697e0"))
                .thresholdVisible(true)
                .shadowsEnabled(true)
                .gradientBarEnabled(true)
                .padding(new Insets(0, 10, 0, 10))
                .gradientBarStops(new Stop(0.00, Color.BLUE),
                                  new Stop(0.25, Color.CYAN),
                                  new Stop(0.50, Color.LIME),
                                  new Stop(0.75, Color.YELLOW),
                                  new Stop(1.00, Color.web("#D6264F")))
                .build();
		
		distance = GaugeBuilder.create()
                .skinType(SkinType.DASHBOARD)
                .prefSize(150, 150)
                .value(100)
                .minValue(0)
                .maxValue(200)
                .animated(true)
                .checkThreshold(true)
                .onThresholdExceeded(e -> System.out.println("Security Distance exceeded"))
                .thresholdVisible(true)
                .interactive(true)
                .threshold(40)
                .title("Distance")
                .unit("Cm")
                .sections(new Section(0, 60, Color.web("#D6264F")),
                          new Section(60, 100, Color.YELLOW),
                          new Section(100, 200, Color.LIME))
                .sectionsVisible(true)
                .build();
		
		
		battery = GaugeBuilder.create()
                .skinType(SkinType.BATTERY)
                .value(40)
                .animated(true)
                .sectionsVisible(true)
                .padding(new Insets(0, 30, 0, 30))
                .sections(new Section(0, 10, Color.rgb(200, 0, 0, 0.8)),
                          new Section(10, 30, Color.rgb(200, 200, 0, 0.8)),
                          new Section(30, 100, Color.rgb(0, 200, 0, 0.8)))
                .build();				
		
		splitPane4RightPane.getItems().addAll(barometer, horizon, battery);
		splitPane4RightPane.setDividerPositions(0.3f, 0.6f, 0.9f);
		splitPane4RightPane.setStyle("-fx-background-color: #242424;");
		
		return splitPane4RightPane;
	}
	
	// Center Node
	public Node createTelemetryCenter(Preferences preferences) {		
		gridFirmware = new GridPane();
		//grid.setGridLinesVisible(true);
		gridFirmware.setVgap(20);
		gridFirmware.setHgap(10);
		gridFirmware.setPadding(new Insets(30,10,10,30));
		
		// Row 0
		Text txtHeader = new Text("Firmware Flasher");	
		txtHeader.setFont(Font.font("Helvetica", 20));
		txtHeader.setFill(Color.rgb(6, 151, 224));		
		// Row Index
		GridPane.setConstraints(txtHeader, 0,0);
		// Span
		GridPane.setColumnSpan(txtHeader, 2);		
		
		// Row 1
		Text txtWifi = new Text("Option 1");
		txtWifi.setFont(Font.font("Helvetica", 16));
		txtWifi.setFill(Color.DARKSLATEGREY);
		// Toggle Button
        switch1 = new OnOffSwitch();
        switch1.setPrefSize(50, 25);
        switch1.setUserData("option1");
        switch1.setToggleGroup(telemetrySwitchToggleGroup);
        switch1.setSwitchColor(Color.DARKSLATEGREY);
        switch1.setThumbColor(Color.rgb(6, 151, 224));
    	// Row Index
		GridPane.setConstraints(txtWifi, 0, 1);
		GridPane.setConstraints(switch1, 1,1);
					
		// Row 2
		Text txtXBee = new Text("Option 2");
		txtXBee.setFont(Font.font("Helvetica", 16));
		txtXBee.setFill(Color.DARKSLATEGREY);
		// Toggle Button
		switch2 = new OnOffSwitch();
		switch2.setUserData("option2");
		switch2.setPrefSize(50, 25);
		switch2.setToggleGroup(telemetrySwitchToggleGroup);
		switch2.setSwitchColor(Color.DARKSLATEGREY);
		switch2.setThumbColor(Color.rgb(6, 151, 224));
		// Row Index
		GridPane.setConstraints(txtXBee, 0, 2);
		GridPane.setConstraints(switch2, 1, 2);
		
		// Row 3
		Text txtNordic = new Text("Option 3");
		txtNordic.setFont(Font.font("Helvetica", 16));
		txtNordic.setFill(Color.DARKSLATEGREY);
		// Toggle Button
		switch3 = new OnOffSwitch();
		switch3.setPrefSize(50, 25);
		switch3.setUserData("option3");
		switch3.setToggleGroup(telemetrySwitchToggleGroup);
		switch3.setSwitchColor(Color.DARKSLATEGREY);
		switch3.setThumbColor(Color.rgb(6, 151, 224));
		// Row Index
		GridPane.setConstraints(txtNordic, 0, 3);
		GridPane.setConstraints(switch3, 1, 3);
				
		if(preferences!=null){
			// Check for a selected connection in Telemetry Switch,
			// if it's find one, then sets the switch found.
			if(preferences.get("CONNTYPE", null) != null){
				// Compare the saved preferences with user data Switches
				String connType = preferences.get("CONNTYPE", null);
				if(connType!=null){
					if(connType.equals("option1")){
						switch1.setSelected(true);
					} else if(connType.equals("option2")){
						switch2.setSelected(true);
					} else if(connType.equals("option3")){
						switch3.setSelected(true);
					}
				}
			}
		}
		
		
		gridFirmware.getChildren().addAll(txtHeader, 
				txtWifi, 
				txtXBee, 
				txtNordic, 
				switch1, 
				switch2, 
				switch3);
		
		return gridFirmware;
	}
	

	public void addRows4Wifi(String host, Integer i){
		labelHost = new Label(I18n.CONFIG.getString("label.host"));
		GridPane.setConstraints(labelHost, 0, 6);
		tfHost = new TextField();
		tfHost.setPrefColumnCount(10);
		GridPane.setConstraints(tfHost, 1, 6);
		labelPort = new Label(I18n.CONFIG.getString("label.port"));
		GridPane.setConstraints(labelPort, 0, 7);
		tfPort = new TextField();
		//tfPort.setTextFormatter(new TextFormatter<>(new NumberStringConverter()));
		tfPort.setPrefColumnCount(5);
		GridPane.setConstraints(tfPort, 1, 7);
		btnClearWifi = new Button(I18n.CONFIG.getString("button.clear"));
		btnClearWifi.setStyle("-fx-base: #D6264F;");
		GridPane.setConstraints(btnClearWifi, 0, 8);
		btnSubmitWifi = new Button(I18n.CONFIG.getString("button.save"));
		btnSubmitWifi.setStyle("-fx-base: #D6264F;");
		GridPane.setConstraints(btnSubmitWifi, 1, 8);
		
		if (host!=null && i!=null){
			tfHost.setText(host);
			tfPort.setText(String.valueOf(i));
		}
		
		gridFirmware.getChildren().addAll(labelHost, tfHost, labelPort, tfPort, btnClearWifi, btnSubmitWifi);
	}
	
	public void removeRows4Wifi(){
		gridFirmware.getChildren().removeAll(labelHost, tfHost, labelPort, tfPort, btnClearWifi, btnSubmitWifi);
	}
	
	
	// EVENTS  //	
	/** 
	 *  If the Telemetry Toggle Button is clicked execute a method
	 *	in the Controller named event4btnTelemetry
	 *
	 *	@param event4btnTelemetry
	 **/
    void addActionEvent4btnTelemetry(EventHandler<ActionEvent> event4btnTelemetry){
    	btnTelemetry.setOnAction(event4btnTelemetry);
    }
    
    
    /** 
	 *  If the Preferences Toggle Button is clicked execute a method
	 *	in the Controller named event4btnPref
	 *
	 *	@param event4btnPref
	 **/
    void addActionEvent4btnPreferences(EventHandler<ActionEvent> event4btnPref){
    	btnPreferences.setOnAction(event4btnPref);
    }
    
    
     /**
      * Added Change Listener to telemetryGroupToolBar.
      * 
      * @param changeListener4GroupToolBar
      */
     void addListener4TelemetrySwitch(ChangeListener<Toggle> changeListener4TelemetrySwitch) {
     	telemetrySwitchToggleGroup.selectedToggleProperty().addListener(changeListener4TelemetrySwitch);
     }
     
    
     /** 
 	 *  If the Clear Button is clicked execute a method
 	 *	in the Controller named event4btnClear
 	 *
 	 *	@param event4btnClear
 	 **/
     void addActionEvent4btnClearWifi(EventHandler<ActionEvent> event4btnClear){
     	btnClearWifi.setOnAction(event4btnClear);
     }
     
     /** 
 	 *  If the Submit Wifi Button is clicked execute a method
 	 *	in the Controller named event4btnSubmitWifi
 	 *
 	 *	@param event4btnSubmitWifi
 	 **/
     void addActionEvent4btnSaveWifi(EventHandler<ActionEvent> event4btnSubmitWifi){
     	btnSubmitWifi.setOnAction(event4btnSubmitWifi);
     }
     
}
