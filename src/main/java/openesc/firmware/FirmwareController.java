package openesc.firmware;
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * @author Javier Hernández
 * 
 **/
import java.util.logging.Logger;

import openesc.OpenESCApp;
import openesc.OpenESCView;

public class FirmwareController {
	private final static Logger LOGGER = Logger.getLogger(FirmwareController.class.getName());

	private FirmwareView fview;
	private OpenESCApp app;
	private OpenESCView view;
	private FirmwareModel f_model;

	public FirmwareController(FirmwareModel f_model, OpenESCView openESCView, OpenESCApp openESCApp) {
		this.app = openESCApp;
		this.view = openESCView;
		this.f_model = f_model;
	}
	
	public void setFirmwareView(FirmwareView fview) {
		this.fview = fview;
	}

	public void addFirmwareEvents() {
		System.out.println("Añadidos los eventos para Firmware Flasher.");
		
	}
}
