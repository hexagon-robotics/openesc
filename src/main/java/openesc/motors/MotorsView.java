package openesc.motors;
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * @author Javier Hernández
 * 
 **/
import java.util.Random;
import java.util.concurrent.Delayed;
import java.util.logging.Logger;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.tools.FlowGridPane;
import javafx.animation.AnimationTimer;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import openesc.firmware.FirmwareView;

public class MotorsView {
	private final static Logger LOGGER = Logger.getLogger(FirmwareView.class.getName());
	
	private static final    Random RND = new Random();
	private Tile gaugeSparkLineTile1;
	private Tile gaugeSparkLineTile2;
	private Tile gaugeSparkLineTile3;
	private Tile gaugeSparkLineTile4;
	private Tile sliderTile1;
	private Tile sliderTile2;
	private Tile sliderTile3;
	private Tile sliderTile4;
	private Tile buttonTile1;
	private Tile buttonTile2;
	private Tile buttonTile3;
	private Tile buttonTile4;

	private long            lastTimerCall;
	private AnimationTimer  timer;


	public Node createMotorsGauges(){
		
				// Create the Tile
				gaugeSparkLineTile1 = TileBuilder.create()
	            .skinType(SkinType.GAUGE_SPARK_LINE)
	            .prefSize(200,250)
	            .title("Motor 1")
	            .animated(true)
	            .textVisible(false)
	            .averagingPeriod(60)
	            .autoScale(true)
	            .autoReferenceValue(true)
	            .barColor(Tile.YELLOW_ORANGE)
	            .barBackgroundColor(Color.rgb(255, 50, 255, 0.1))
	            .sections(new eu.hansolo.tilesfx.Section(0, 33, Tile.LIGHT_GREEN),
	                      new eu.hansolo.tilesfx.Section(33, 67, Tile.YELLOW),
	                      new eu.hansolo.tilesfx.Section(67, 100, Tile.LIGHT_RED))
	            .sectionsVisible(true)
	            .highlightSections(true)
	            .strokeWithGradient(true)
	            .gradientStops(new Stop(0.0, Tile.LIGHT_GREEN),
	                           new Stop(0.33, Tile.LIGHT_GREEN),
	                           new Stop(0.33,Tile.YELLOW),
	                           new Stop(0.67, Tile.YELLOW),
	                           new Stop(0.67, Tile.LIGHT_RED),
	                           new Stop(1.0, Tile.LIGHT_RED))
	            .build();
				
		        sliderTile1 = TileBuilder.create()
                        .skinType(SkinType.SLIDER)
                        .prefSize(200, 200)
                        .description("Speed")
                        .minValue(1000)
                        .maxValue(2000)
                        .barBackgroundColor(Tile.FOREGROUND)
                        .build();
		        
				
				buttonTile1 = TileBuilder.create()
                        .skinType(SkinType.CUSTOM)
                        .prefSize(200, 100)
                        .graphic(new Button("Flash now"))
                        .roundedCorners(false)
                        .build();
				
				gaugeSparkLineTile2 = TileBuilder.create()
			            .skinType(SkinType.GAUGE_SPARK_LINE)
			            .prefSize(200,250)
			            .title("Motor 2")
			            .animated(true)
			            .textVisible(false)
			            .averagingPeriod(60)
			            .autoReferenceValue(true)
			            .barColor(Tile.YELLOW_ORANGE)
			            .barBackgroundColor(Color.rgb(255, 50, 255, 0.1))
			            .sections(new eu.hansolo.tilesfx.Section(0, 33, Tile.LIGHT_GREEN),
			                      new eu.hansolo.tilesfx.Section(33, 67, Tile.YELLOW),
			                      new eu.hansolo.tilesfx.Section(67, 100, Tile.LIGHT_RED))
			            .sectionsVisible(true)
			            .highlightSections(true)
			            .strokeWithGradient(true)
			            .gradientStops(new Stop(0.0, Tile.LIGHT_GREEN),
			                           new Stop(0.33, Tile.LIGHT_GREEN),
			                           new Stop(0.33,Tile.YELLOW),
			                           new Stop(0.67, Tile.YELLOW),
			                           new Stop(0.67, Tile.LIGHT_RED),
			                           new Stop(1.0, Tile.LIGHT_RED))
			            .build();
				

		        sliderTile2 = TileBuilder.create()
                        .skinType(SkinType.SLIDER)
                        .prefSize(200, 200)
                        .description("Speed")
                        .minValue(1000)
                        .maxValue(2000)
                        .barBackgroundColor(Tile.FOREGROUND)
                        .build();
				
				
				buttonTile2 = TileBuilder.create()
                        .skinType(SkinType.CUSTOM)
                        .prefSize(200, 100)
                        .graphic(new Button("Flash now"))
                        .roundedCorners(false)
                        .build();
				
				
				gaugeSparkLineTile3 = TileBuilder.create()
			            .skinType(SkinType.GAUGE_SPARK_LINE)
			            .prefSize(200,250)
			            .title("Motor 3")
			            .animated(true)
			            .textVisible(false)
			            .averagingPeriod(60)
			            .autoReferenceValue(true)
			            .barColor(Tile.YELLOW_ORANGE)
			            .barBackgroundColor(Color.rgb(255, 50, 255, 0.1))
			            .sections(new eu.hansolo.tilesfx.Section(0, 33, Tile.LIGHT_GREEN),
			                      new eu.hansolo.tilesfx.Section(33, 67, Tile.YELLOW),
			                      new eu.hansolo.tilesfx.Section(67, 100, Tile.LIGHT_RED))
			            .sectionsVisible(true)
			            .highlightSections(true)
			            .strokeWithGradient(true)
			            .gradientStops(new Stop(0.0, Tile.LIGHT_GREEN),
			                           new Stop(0.33, Tile.LIGHT_GREEN),
			                           new Stop(0.33,Tile.YELLOW),
			                           new Stop(0.67, Tile.YELLOW),
			                           new Stop(0.67, Tile.LIGHT_RED),
			                           new Stop(1.0, Tile.LIGHT_RED))
			            .build();
				

		        sliderTile3 = TileBuilder.create()
                        .skinType(SkinType.SLIDER)
                        .prefSize(200, 200)
                        .description("Speed")
                        .minValue(1000)
                        .maxValue(2000)
                        .barBackgroundColor(Tile.FOREGROUND)
                        .build();
				
				
				buttonTile3 = TileBuilder.create()
                        .skinType(SkinType.CUSTOM)
                        .prefSize(200, 100)
                        .graphic(new Button("Flash now"))
                        .roundedCorners(false)
                        .build();
				
				gaugeSparkLineTile4 = TileBuilder.create()
			            .skinType(SkinType.GAUGE_SPARK_LINE)
			            .prefSize(200,250)
			            .title("Motor 4")
			            .animated(true)
			            .textVisible(false)
			            .averagingPeriod(60)
			            .autoReferenceValue(true)
			            .barColor(Tile.YELLOW_ORANGE)
			            .barBackgroundColor(Color.rgb(255, 50, 255, 0.1))
			            .sections(new eu.hansolo.tilesfx.Section(0, 33, Tile.LIGHT_GREEN),
			                      new eu.hansolo.tilesfx.Section(33, 67, Tile.YELLOW),
			                      new eu.hansolo.tilesfx.Section(67, 100, Tile.LIGHT_RED))
			            .sectionsVisible(true)
			            .highlightSections(true)
			            .strokeWithGradient(true)
			            .gradientStops(new Stop(0.0, Tile.LIGHT_GREEN),
			                           new Stop(0.33, Tile.LIGHT_GREEN),
			                           new Stop(0.33,Tile.YELLOW),
			                           new Stop(0.67, Tile.YELLOW),
			                           new Stop(0.67, Tile.LIGHT_RED),
			                           new Stop(1.0, Tile.LIGHT_RED))
			            .build();
				

		        sliderTile4 = TileBuilder.create()
                        .skinType(SkinType.SLIDER)
                        .prefSize(200, 200)
                        .description("Speed")
                        .minValue(1000)
                        .maxValue(2000)
                        .barBackgroundColor(Tile.FOREGROUND)
                        .build();
				
				
				buttonTile4 = TileBuilder.create()
                        .skinType(SkinType.CUSTOM)
                        .prefSize(200, 100)
                        .graphic(new Button("Flash now"))
                        .roundedCorners(false)
                        .build();
	    
		
		lastTimerCall = System.nanoTime();
		timer = new AnimationTimer() {
	       @Override public void handle(long now) {
	           if (now > lastTimerCall + 3_500_000_000L) {
                	gaugeSparkLineTile1.setValue(RND.nextDouble() * 100);
                	gaugeSparkLineTile2.setValue(RND.nextDouble() * 100);
                	gaugeSparkLineTile3.setValue(RND.nextDouble() * 100);
                	gaugeSparkLineTile4.setValue(RND.nextDouble() * 100);
        }
	       }
		};
				
				
	    FlowGridPane pane = new FlowGridPane(4, 3,  
	    		gaugeSparkLineTile1,
	    		gaugeSparkLineTile2,
	    		gaugeSparkLineTile3,
	    		gaugeSparkLineTile4,
	    		sliderTile1,
	    		sliderTile2,
	    		sliderTile3,
	    		sliderTile4,
	    		buttonTile1, 
	    		buttonTile2,
	    		buttonTile3,
	    		buttonTile4);
	    
	    timer.start();
	    
	    
	    pane.setHgap(5);
        pane.setVgap(5);
        //pane.setAlignment(Pos.CENTER);
        pane.setCenterShape(true);
        pane.setPadding(new Insets(5));
        //pane.setPrefSize(800, 600);
        pane.setBackground(new Background(new BackgroundFill(Color.web("#101214"), CornerRadii.EMPTY, Insets.EMPTY)));
        
        
		return pane;
	}
}
