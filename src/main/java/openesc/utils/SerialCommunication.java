package openesc.utils;
import java.io.PrintWriter;
import java.util.Scanner;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;


public class SerialCommunication {

	private SerialPort port = null;
    static boolean received;

	/**
	 * This method opens the COM port with port parameters: Baudrate: selected;
	 * databits: 8; Stopbit: 1; parity: none;
	 * 
	 * @param selectedPort
	 * @param selectedBaudrate
	 */
	public void connect(SerialPort selectedPort) {
			this.port = selectedPort;
			// Open connection
			port.setBaudRate(port.getBaudRate());
			port.setNumDataBits(8);
			port.setNumStopBits(SerialPort.ONE_STOP_BIT);
			port.setParity(SerialPort.NO_PARITY);
			port.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
	        System.out.println("Open port: " + port.openPort());
			Scanner in = new Scanner(port.getInputStream());
	        PrintWriter out = new PrintWriter(port.getOutputStream(),true);
	        port.addDataListener(new SerialPortDataListener() {
	            @Override
	            public int getListeningEvents() {
	                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
	            }

	            @Override
	            public void serialEvent(SerialPortEvent serialPortEvent) {
	                String input = "";

	               input = in.nextLine();

	                System.out.println("return: " + input);
	                received=true;
	            }
	        });


	int counter =0;
	        while(!received) {
	            System.out.println(counter);
	            out.println(counter);
	            out.flush();
	            try {
	                Thread.sleep(20);
	            } catch (InterruptedException e) {
	                e.printStackTrace();
	            }
	            counter++;
	        }
	        out.println('w');
	        System.out.println("w");
	           /*     String input = in.nextLine();
	                System.out.println("return: "+input+input.isEmpty());*/	    	        
		
		}
	}

