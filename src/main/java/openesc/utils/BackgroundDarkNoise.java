package openesc.utils;

import eu.hansolo.enzo.common.Util;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.paint.Color;

public class BackgroundDarkNoise {
	
	public Background createHBackground(int WIDTH, int HEIGTH){
		
		Image image = Util.createGrayNoise(WIDTH, HEIGTH, Color.rgb(145, 145, 145), Color.rgb(160, 160, 160));
		BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
		BackgroundImage backgroundImage = new BackgroundImage(image, 
											BackgroundRepeat.REPEAT,
											BackgroundRepeat.NO_REPEAT,
											BackgroundPosition.CENTER,
											backgroundSize);
		Background background = new Background(backgroundImage);
		
		return background;
	}
	
public Background createVBackground(int WIDTH, int HEIGTH){
		
		Image image = Util.createGrayNoise(WIDTH, HEIGTH, Color.rgb(145, 145, 145), Color.rgb(160, 160, 160));
		BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
		BackgroundImage backgroundImage = new BackgroundImage(image, 
											BackgroundRepeat.NO_REPEAT,
											BackgroundRepeat.REPEAT,
											BackgroundPosition.CENTER,
											backgroundSize);
		Background background = new Background(backgroundImage);
		
		return background;
	}
}
