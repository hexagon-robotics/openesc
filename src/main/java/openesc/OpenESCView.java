package openesc;
/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 * @author Javier Hernández
 * 
 **/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Properties;

import javax.management.Notification;

import com.fazecast.jSerialComm.SerialPort;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.utils.FontAwesomeIconFactory;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.utils.MaterialIconFactory;
import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import openesc.utils.I18n;

/**
 * 
 * * * * * * *
 * OpenESC *
 * * * * * * *
 * 
 * Copyright [2019] [OpenESC]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *   
 * @author Javier Hernández
 * 
 **/
public class OpenESCView extends BorderPane {
	private ToolBar hToolBar;
	private ToggleGroup hGroupToolBar;
	private ToggleGroup groupMenu;
	
	// ToolBar Controls
	private ToggleButton btnHelp;
	private static ToggleButton btnFirmware;
	public ToggleButton getBtnFirmware() {
		return btnFirmware;
	}

	private static ToggleButton btnLanguage;
	private static ToggleButton btnSetup;
	private static ToggleButton btnMotors;
	private static ToggleButton btnAdjustments;

	private static ToggleButton connButton;
	private Tile connectionTile;
	private ComboBox comboPorts;
	private ComboBox comboRates;
	private CheckBox manualMode;
	
	
	// Menu Style
	private final String IDLE_BUTTON_STYLE = "-fx-background-color: #444444;"
			+ "-fx-pref-width: 200";
	private final String HOVERED_BUTTON_STYLE = "-fx-background-color: #545454;"
    		+ "-fx-shadow-highlight-color: #CCFF99;"
    		+ "-fx-outer-border: 2;"
    		+ "-fx-inner-border: 2;"
    		+ "-fx-body-color: transparent;"
    		+ "-fx-pref-width: 200;";
	
	
	
    public String getHOVERED_BUTTON_STYLE() {
		return HOVERED_BUTTON_STYLE;
	}
		
	OpenESCView() {
		hGroupToolBar = new ToggleGroup();
		groupMenu = new ToggleGroup();
		this.setTop(appToolBAr());
		this.setLeft(appMenu());
		this.setStyle("-fx-background-color: #121212;");
	}

	// Nodes
	private Node appToolBAr() {		
		// Logo app
		ImageView logo = new ImageView();
		Image image = new Image(OpenESCApp.class.getResourceAsStream("graphics/logo_OpenESC_186x35.png"));
		logo.setImage(image);
		
		/*Create buttons to Top ToolBar*/        
        // Connection Button
        connButton = MaterialIconFactory.get().createIconToggleButton(
    			MaterialIcon.USB,
    			I18n.COMMON.getString("button.connect").toUpperCase(), 
        		"2em", 
        		"0.8em", 
        		ContentDisplay.TOP);
        setAppfStyle(connButton);        
        
        
        SerialPort[] comPorts = SerialPort.getCommPorts();
		ArrayList<SerialPort> comPortsList = new ArrayList<>(Arrays.asList(comPorts));
		ListIterator<SerialPort> portIterator = comPortsList.listIterator();

        ObservableList<String> ports = 
			    FXCollections.observableArrayList();
        
	    while (portIterator.hasNext()) { 
           //System.out.print(portIterator.next().getPortDescription() + " ");
           ports.add(portIterator.next().getPortDescription().toString());
       } 
    	
		comboPorts = new ComboBox(ports);
		if(comboPorts.getValue()!=null) {
			comboPorts.setValue(ports.get(0));
		}
		//comboPorts.setEditable(true);
		comboPorts.setPrefWidth(200);
		comboPorts.setStyle("-fx-background-color: #646464;" +
				"-fx-text-fill: white;" +
				"-fx-border-color: #202020;");
		
		Label lv = new Label("Port");
		lv.setTextFill(Color.WHITE);					
		
		
		ObservableList<Integer> baudrates = 
			    FXCollections.observableArrayList(
			        4800,
			        9600,
			        14400,
			        19200,
			        28800,
			        38400,
			        57600,
			        115200);
		
		comboRates = new ComboBox(baudrates);
		comboRates.setValue(baudrates.get(7));
		comboRates.setEditable(true);
		comboRates.setPrefWidth(150);
		comboRates.setStyle("-fx-background-color: #646464;" +
							"-fx-border-color: #202020;");
				
		
		Label la = new Label("Baud Rate");
		la.setTextFill(Color.WHITE);
		
		manualMode = new CheckBox("Manual Mode");
		manualMode.setStyle("-fx-background-color: #646464;" +
				"-fx-border-color: #202020;");
		
		GridPane conn_grid_pane = new GridPane();
		conn_grid_pane.setVgap(1);
		conn_grid_pane.setHgap(5);
		conn_grid_pane.setPadding(new Insets(0,0,0,0));
		conn_grid_pane.add(lv, 0, 0);
		conn_grid_pane.add(comboPorts, 1, 0);
		//conn_grid_pane.add(la, 0, 1);
		conn_grid_pane.add(manualMode, 1, 1);
		
		connectionTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(245.0, 60.0)
                .graphic(conn_grid_pane)
                .roundedCorners(true)
                .build();
        
        final Pane logoSpacer = new Pane();
        logoSpacer.setStyle("-fx-background-color: #121212;");
        logoSpacer.getChildren().add(logo);
        HBox.setHgrow(logoSpacer, Priority.SOMETIMES);
        hToolBar = new ToolBar();
        hToolBar.getItems().addAll(
        		logoSpacer,
        		connectionTile,
        		connButton);
        hToolBar.setStyle("-fx-border-style: dashed;\n"
                + "-fx-border-color: transparent;"
                + "-fx-background-color: transparent;\n");

		return hToolBar;
	}
	
	private Node appMenu() {
		final String iconSize = "1.5em";	
		final String fontSize = "1.5em";
		
        // Help Button
        btnHelp = FontAwesomeIconFactory.get().createIconToggleButton(
        		FontAwesomeIcon.QUESTION, 
        		I18n.COMMON.getString("button.help").toUpperCase(), 
        		iconSize, 
        		fontSize, 
        		ContentDisplay.LEFT);
        setAppfMenuStyle(btnHelp);
        
		/*
		 * // Donate Button btnDonate =
		 * FontAwesomeIconFactory.get().createIconToggleButton( FontAwesomeIcon.PAYPAL,
		 * I18n.COMMON.getString("button.donate").toUpperCase(), iconSize, fontSize,
		 * ContentDisplay.LEFT); setAppfMenuStyle(btnDonate);
		 */
        
        // Language Button
        btnLanguage = FontAwesomeIconFactory.get().createIconToggleButton(
        		FontAwesomeIcon.LANGUAGE, 
        		I18n.COMMON.getString("button.language").toUpperCase(), 
        		iconSize, 
        		fontSize, 
        		ContentDisplay.LEFT);
        setAppfMenuStyle(btnLanguage);
		
        // Firmware Button
		btnFirmware = FontAwesomeIconFactory.get().createIconToggleButton(
				FontAwesomeIcon.DOWNLOAD	,
				I18n.COMMON.getString("button.menu.firmware").toUpperCase(),
				iconSize,
				fontSize,
				ContentDisplay.LEFT);
		btnFirmware.setTextFill(javafx.scene.paint.Color.web("#242424"));
		setAppfMenuStyle(btnFirmware);
		
		// Setup Button
		btnSetup = FontAwesomeIconFactory.get().createIconToggleButton(
				FontAwesomeIcon.COGS,
				I18n.COMMON.getString("button.menu.setup").toUpperCase(),
				iconSize,
				fontSize,
				ContentDisplay.LEFT);
		setAppfMenuStyle(btnSetup);
		
		// Adjustments Button
		btnAdjustments =  FontAwesomeIconFactory.get().createIconToggleButton(
				FontAwesomeIcon.SLIDERS,
				I18n.COMMON.getString("button.menu.adjustments").toUpperCase(),
				iconSize,
				fontSize,
				ContentDisplay.LEFT);
		setAppfMenuStyle(btnAdjustments);
		
		// Motors Button
		btnMotors = FontAwesomeIconFactory.get().createIconToggleButton(
				FontAwesomeIcon.ARROWS_ALT,
				I18n.COMMON.getString("button.menu.motors").toUpperCase(),
				iconSize,
				fontSize,
				ContentDisplay.LEFT);
		setAppfMenuStyle(btnMotors);
		
		disableMenuButtons(true);
		
		VBox menu = new VBox();
		menu.setId("menu");
		menu.prefHeightProperty().bind(this.heightProperty());
		menu.setPrefWidth(200);
		menu.setStyle("-fx-background-color: #121212;" + 
					"-fx-border-color: #202020;");

		menu.getChildren().addAll(
				btnSetup,
				appMenuSeparator(),
				btnAdjustments,
				appMenuSeparator(),
				btnMotors,
				appMenuSeparator(),
				btnHelp,
				appMenuSeparator(),
				btnLanguage);
		
		return menu;
	}
	
	// Common Style for the Menus
		private void setAppfStyle(ToggleButton menu) {
	        menu.setTextFill(javafx.scene.paint.Color.web("#848484"));
			menu.setStyle(IDLE_BUTTON_STYLE);
			menu.getStyleClass().add("app-menu-icon");
			
			menu.setOnMouseEntered(new EventHandler<Event>() {
		    	@Override
		    	public void handle(Event event) {
		    		menu.setStyle(HOVERED_BUTTON_STYLE);
		    	}
			});
			
			menu.setOnMouseExited(new EventHandler<Event>() {
		    	@Override
		    	public void handle(Event event) {
		    		menu.setStyle(IDLE_BUTTON_STYLE);
		    	}
			});
		}
		
	
	// Style for the Vertical Menu
	private void setAppfMenuStyle(ToggleButton menu) {
        menu.setTextFill(javafx.scene.paint.Color.web("#848484"));
        menu.setAlignment(Pos.BASELINE_LEFT);
		menu.setStyle(IDLE_BUTTON_STYLE);
		menu.getStyleClass().add("app-menu-icon");
		
		menu.setOnMouseEntered(new EventHandler<Event>() {
	    	@Override
	    	public void handle(Event event) {
	    		menu.setStyle(HOVERED_BUTTON_STYLE);
	    	}
		});
		
		menu.setOnMouseExited(new EventHandler<Event>() {
	    	@Override
	    	public void handle(Event event) {
	    		menu.setStyle(IDLE_BUTTON_STYLE);
	    	}
		});
	}
	
	
	// EVENTS  //	    
    /**
     * If the Help Button is clicked execute a method
     * in the Controller named event4btnHelp.
     * 
     * @param event4btnHelp
     */
    void addActionEvent4btnHelp(EventHandler<ActionEvent> event4btnHelp){
    	btnHelp.setOnAction(event4btnHelp);
    }
    

    /**
     * If the Language Button is clicked execute a method
     * in the Controller named event4btnLanguage. 
     * 
     * @param event4btnLanguage
     */
    void addActionEvent4btnLanguage(EventHandler<ActionEvent> event4btnLanguage){
    	btnLanguage.setOnAction(event4btnLanguage);
    }
    
    /**
     * If the SETUP Button is clicked execute a method
     * in the Controller named event4btnSetup. 
     * 
     * @param event4btnSetup
     */
    void addActionEvent4btnSetup(EventHandler<ActionEvent> event4btnSetup){
    	btnSetup.setOnAction(event4btnSetup);
    }
    
    /**
     * If the Adjustments Button is clicked execute a method
     * in the Controller named event4btnAdjustments. 
     * 
     * @param event4btnAdjustments
     */
    void addActionEvent4btnAdjustments(EventHandler<ActionEvent> event4btnAdjustments){
    	btnAdjustments.setOnAction(event4btnAdjustments);
    }
    
    
    /**
     * If the Motors is clicked execute a method
     * in the Controller named event4btnMotors. 
     * 
     * @param event4btnMotors
     */
    void addActionEvent4btnMotors(EventHandler<ActionEvent> event4btnMotors){
    	btnMotors.setOnAction(event4btnMotors);
    }
    
    /**
     * If the Firmware Button is clicked execute a method
     * in the Controller named event4btnFirmware. 
     * 
     * @param event4btnFirmware
     */
    void addActionEvent4btnFirmware(EventHandler<ActionEvent> event4btnFirmware){
    	btnFirmware.setOnAction(event4btnFirmware);
    }
    
    
    /**
     * If the Connection Button is clicked execute a method
     * in the Controller named event4btnConnect.
     * 
     * @param event4btnConnect
     */
    void addActionEvent4btnConnection(EventHandler<ActionEvent> event4btnConnect){
    	connButton.setOnAction(event4btnConnect);
    }
    
    
    /**
     * Added Change Listener to Horizontal GroupToolBar.
     * 
     * @param changeListener4hGroupToolBar
     */
    void addListener4GroupToolBar(ChangeListener<Toggle> changeListener4hGroupToolBar) {
    	hGroupToolBar.selectedToggleProperty().addListener(changeListener4hGroupToolBar);
    }
    
    /**
     * Added Change Listener to Menu.
     * 
     * @param changeListener4Menu
     */
    void addListener4Menu(ChangeListener<Toggle> changeListener4Menu) {
    	groupMenu.selectedToggleProperty().addListener(changeListener4Menu);
    }
    

    /** Enable/Disable Toggle Buttons **/
    public static void disableMenuButtons(boolean b) {
		btnMotors.setDisable(b);
		btnAdjustments.setDisable(b);
		btnSetup.setDisable(b);
	}

	/** Hide/Show Toggle buttons **/
    public static void showMenuButtons(boolean b) {
		btnMotors.setVisible(b);
		btnAdjustments.setVisible(b);
		btnSetup.setVisible(b);
	}
    
    
	// **************** Dialogs *************** //
	/**
	 * Show a Dialog to select the language and 
	 * write selection to file.
	**/
	public void showLangDialog() {
		List<String> languages = new ArrayList<>();
		languages.add("English");
		languages.add("Spanish");
		languages.add("French");
		languages.add("Italian");
		languages.add("German");
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>("English", languages);
		dialog.setTitle(I18n.COMMON.getString("title.dialog.lang"));
		dialog.setHeaderText(I18n.COMMON.getString("header.dialog.lang"));
		dialog.setContentText(I18n.COMMON.getString("content.dialog.lang"));
				
		String selected = null;
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
			selected = result.get();
			
			// Write selection to file "lang.properties"
			File langFile = new File("lang.properties");
			Properties props = new Properties();
			props.setProperty("lang", selected);
			FileWriter writer;
			try {
				writer = new FileWriter(langFile);
				props.store(writer, null);
			} catch (FileNotFoundException ex){
				ex.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Show dialog to confirm the operation
			Alert alertLang = new Alert(AlertType.CONFIRMATION);
			alertLang.setTitle(I18n.COMMON.getString("alert.lang.title"));
			alertLang.setContentText(I18n.COMMON.getString("alert.lang.content"));
			Optional<ButtonType> result1 = alertLang.showAndWait();
			if(result1.get() == ButtonType.OK){
				System.exit(0);
			} 
		}
	}
	
	/**
	 * Show a Dialog if is not selected any connection.
	 * @param roverConfigView 
	 */
	public void showConnDialog(OpenESCView skyView) {
		Alert alertConn = new Alert(AlertType.WARNING);
		alertConn.setTitle(I18n.COMMON.getString("alertconn.title"));
		alertConn.setContentText(I18n.COMMON.getString("alertconn.content"));
		Optional<ButtonType> accept = alertConn.showAndWait();
		if(accept.get() == ButtonType.OK){
			//connButton.setStatus(Status.DESELECTED);
		}
	}
	
	
	/**
	 * Show a Notification for connection status.
	 * @param title
	 * @param message
	 * @param icon 
	 */
	public void showSocketNotification(String title, String message){ 
		final Notification notification;
		/*
		 * Notification.Notifier notifier;
		 * 
		 * Notifier.INSTANCE.setAlwaysOnTop(false);
		 * 
		 * notification = NotificationBuilder.create() .title(title) .message(message)
		 * .image(Notification.SUCCESS_ICON) .build();
		 * 
		 * notifier = NotifierBuilder.create().build(); notifier.notify(notification);
		 */
	}
    
	
	private Separator appToolBarSeparator() {
    	Separator separator = new Separator();
    	separator.setVisible(false);
    	separator.setPadding(new Insets(0,10,0,10));
    	separator.setStyle("-fx-background-color: transparent;");
    	separator.setStyle("-fx-shape: dashed;");
    	
    	return separator;
    }	
	
	private Separator appMenuSeparator() {
    	Separator separator = new Separator();
    	separator.setVisible(false);
    	separator.setStyle("-fx-padding: 0 0 0 0;" + 
                "-fx-border-style: solid;" + 
                "-fx-border-width: 0 0 0.2 0;" +
                "-fx-border-radius: 0;" +
                "-fx-background-color: red;" +                
                "-fx-border-color: red;\n");
    	
    	return separator;
    }
	
	
	// GETTERS & SETTERS
	public ComboBox getComboPorts() {
		return comboPorts;
	}
	
	public ComboBox getComboRates() {
		return comboRates;
	}
}
