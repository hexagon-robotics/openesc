# OpenESC for Desktop
This is the version of OpenESC for Desktop.


# OpenESC on Windows
You NOT need have installed Gradle.

If you want to know more about Gradle you can visit this web page > https://gradle.org/whygradle-build-automation/

You need have installed Oracle JDK http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html?ssSourceSiteId=otnes (or newer).

Then clone the project:

$ git clone https://github.com/hexagon-robotics/openesc.git

Enter in the project directory:

$ cd openesc/

Run OpenESC is pretty simple. You just have that execute this command in a MS-DOS console:

$ start gradlew run



# OpenESC on Linux
You need have installed Gradle 2.12 or later. You can download Gradle here > https://gradle.org/gradle-download/

If you want to know more about Gradle you can visit this web page > https://gradle.org/whygradle-build-automation/

Then clone the project:

$ git clone https://github.com/hexagon-robotics/openesc.git

Enter in the project directory:

$ cd openesc/

Run OpenESC is pretty simple. You just have that execute this command in a console:

$ gradle run

If you want to list all task, you can execute:

$ gradle tasks



# Developers
Install Gradle in eclipse
Import OpenESC as Gradle project
